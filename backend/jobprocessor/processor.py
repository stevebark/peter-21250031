import boto3
import json
import logging
import os
import time
from jobs import process_job

SQS_ENDPOINT_URL = os.getenv('SQS_ENDPOINT_URL', 'http://localhost:4576')
SQS_REGION_NAME = os.getenv('SQS_REGION_NAME', 'us-east-1')

__doc__ = '''
    Simple module to simulate long-running processes

    Work can be invoked by pushing JSON-encoded messages to the 
    SQS queue 'jobs' in the following format:
    
    {
        'job': 'foo',
        'id': 123
    } 
    
    The 'job' and 'id' keys are mandatory. 
    'job' specifies which job type is to be invoked.
    'id' identifies this job and will be supplied in the result.
    
    Results are sent to the 'results' SQS queue in the following format:
    
    Successful job:
    
    The contents of 'result' are job-dependent, but will always be
    a dictionary containing at least one value.
    
    {
        'request': {
            'job': 'foo',
            'id': 123
        },
        'result': {
            'foo': 'bar'
        }
    }
    
    Errored job:
    
    {
        'request': {
            'job': 'foo',
            'id': 123
        },
        'error': {
            'message': 'The job failed'
        }
    }
'''

def get_sqs_kwargs():
    result = {}
    if SQS_ENDPOINT_URL:
        result['endpoint_url'] = SQS_ENDPOINT_URL
    if SQS_REGION_NAME:
        result['region_name'] = SQS_REGION_NAME
    return result

def process_json_message(json_message, invokation_func):
    message_data = {}
    try:
        message_data = json.loads(json_message)
        job_type = message_data['job']
        job_id = message_data['id']
        
        logging.info(f'Processing job: TYPE="{job_type}" ID={job_id}')
        
        job_response = invokation_func(job_type, job_id)
        
        full_result = {
            'request': message_data,
            'result': job_response
        }
        
        return json.dumps(full_result)
    except Exception as e:
        return json.dumps({
            'request': message_data,
            'error': {
                'message': str(e)
            }
        })

def serve():
    started = time.time()
    # Initialise connection to SQS (retry to handle start-up race)
    while True:
        try:
            sqs = boto3.resource('sqs', **get_sqs_kwargs())
            # Create the queues. This returns an SQS.Queue instance
            job_queue = sqs.create_queue(QueueName='jobs')
            result_queue = sqs.create_queue(QueueName='results')
            break
        except Exception as e:
            logging.error(e)
            if time.time() - started > 30:
                raise
            else:
                time.sleep(1)

    # Pull messages from job_queue and process them, pushing results onto result_queue
    while True:
        try:
            for incoming_message in job_queue.receive_messages(WaitTimeSeconds=10, VisibilityTimeout=120):
                try:
                    response_json = process_json_message(incoming_message.body, invokation_func=process_job)
                    result_queue.send_message(MessageBody=response_json)
                    incoming_message.delete()
                except Exception as e:
                    logging.exception(e)
        except Exception as e:
            logging.error(f"Error reading from SQS: {e}")
            time.sleep(1)

if __name__ == '__main__':
    serve()