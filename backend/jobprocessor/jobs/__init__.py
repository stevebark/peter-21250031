from .foo import process_foo
from .bar import process_bar

JOBS = {
    'foo': process_foo,
    'bar': process_bar
}

class JobTypeNotRecognised(Exception):
    pass

def process_job(job_type, job_id):
    try:
        return JOBS[job_type](job_id)
    except KeyError:
        raise JobTypeNotRecognised(f'Job type not recognised: {job_type}')