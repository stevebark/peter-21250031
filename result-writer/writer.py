import os
import json
import time
import logging
import sys
from db import Job, create_session
from sqs import create_queue

logger = logging.getLogger(__name__)
logger.setLevel('INFO')
logger.addHandler(logging.StreamHandler(sys.stdout))

def update_job(response):
    session = create_session()

    job = session.query(Job).filter(Job.id == response['request']['id']).one()
    
    if 'error' in response.keys():
        job.status = 'FAIL'
        job.error = response['error']
    elif 'result' in response.keys():
        job.status = 'SUCCESS'
        job.result = response['result']

    session.commit()

def serve():
    result_queue = create_queue('results')
    # Pull messages from job_queue and process them, pushing results onto result_queue
    while True:
        try:
            logger.info('Checking Queue')
            for incoming_message in result_queue.receive_messages(WaitTimeSeconds=10, VisibilityTimeout=120):
                try:
                    logger.info('Message Received')
                    response = json.loads(incoming_message.body)
                    update_job(response)
                    incoming_message.delete()
                except Exception as e:
                    logging.exception(e)
        except Exception as e:
            logger.error(f"Error reading from SQS: {e}")
            time.sleep(1)

if __name__ == "__main__":
    logger.info("Starting WRITER")
    serve()
