function with_backoff {
  local max_attempts=${MIGRATE_ATTEMPTS-5}
  local timeout=${MIGRATE_TIMEOUT-1}
  local attempt=0
  local exitCode=0

  while [ $attempt -lt $max_attempts ]
  do
    echo "Attepmt $attempt of $max_attempts."

    "$@"
    exitCode=$?

    if [[ $exitCode == 0 ]]
    then
      break
    fi

    echo "Failure! attepmt $attempt of $max_attempts. Retrying in $timeout.."
    sleep $timeout
    attempt=$(( attempt + 1 ))
    timeout=$(( timeout * 2 ))

  done

  if [[ $exitCode != 0 ]]
  then
    echo "You've failed me for the last time!"
  fi

  return $exitCode
}

with_backoff ./flyway -user=$DB_USER -password=$DB_PASSWORD -url=jdbc:mysql://$DB_HOST:$DB_PORT/$DB_NAME migrate