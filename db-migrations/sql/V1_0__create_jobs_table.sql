create table jobs
(
	id int not null
		primary key,
	job varchar(200) not null,
	status varchar(200) not null,
	error json null,
	result json null
);

