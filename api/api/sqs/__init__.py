import boto3
import os
import time

SQS_ENDPOINT_URL = os.getenv('SQS_ENDPOINT_URL', 'http://localhost:4576')
SQS_REGION_NAME = os.getenv('SQS_REGION_NAME', 'us-east-1')

sqs = None

def _get_sqs_kwargs():
    result = {}
    if SQS_ENDPOINT_URL:
        result['endpoint_url'] = SQS_ENDPOINT_URL
    if SQS_REGION_NAME:
        result['region_name'] = SQS_REGION_NAME
    return result

def create_queue(name):
    started = time.time()
    # Initialise connection to SQS (retry to handle start-up race)
    global sqs
    while True:
        try:
            if sqs is None:
                sqs = boto3.resource('sqs', **_get_sqs_kwargs())
            return sqs.create_queue(QueueName=name)
        except Exception as e:
            if time.time() - started > 30:
                raise
            else:
                time.sleep(1)
