import os
from flask import Flask, request, jsonify
import json
import time
import logging
import sys
from sqlalchemy.orm.exc import NoResultFound
from flask_expects_json import expects_json
from db import Job, create_session
from sqs import create_queue

logger = logging.getLogger(__name__)
logger.setLevel('INFO')
logger.addHandler(logging.StreamHandler(sys.stdout))

schema = {
    'type': 'object',
    'properties': {
        'id': {'type': 'integer'},
        'job': {'type': 'string', 'enum': ['foo', 'bar']}
    },
    'required': ['id', 'job']
}

app = Flask(__name__)
job_queue = create_queue('jobs')

def create_pending_response(job):
    return { 'id': job.id, 'job': job.job, 'status': job.status }

def create_success_response(job):
    return { 'id': job.id, 'job': job.job, 'status': job.status, 'result': job.result }

def create_failed_response(job):
    return { 'id': job.id, 'job': job.job, 'status': job.status, 'error': job.error }

def create_job_api_response(job):
    writers = {
        'PENDING': create_pending_response,
        'SUCCESS': create_success_response,
        'FAIL': create_failed_response
    }
    func = writers[job.status]
    return func(job)

@app.route("/<int:id>", methods=["GET"])
def get_job(id):
    try:
        session = create_session()
        job = session.query(Job).filter(Job.id == id).one()
        return jsonify(create_job_api_response(job)), 200
    except NoResultFound:
        return "No Job Found", 404

@app.route("/", methods=["POST"])
@expects_json(schema)
def create_job():

    body = request.get_json()
    logger.info("SAVING TO DB")
    session = create_session()
    job = Job(id=body['id'], job=body['job'], status='PENDING')
    session.add(job)
    session.commit()

    logger.info("SENDING TO QUEUE")
    job_queue.send_message(MessageBody=json.dumps(body))
    return "JOB CREATED", 200

if __name__ == "__main__":
    logger.info("Starting SERVER1")
    app.run(host="0.0.0.0",port=80)