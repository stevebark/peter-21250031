import sys
import os
import unittest
import json
from api.app import create_job_api_response
from api.db import Job

class TestResponsesFixture(unittest.TestCase):
    
    def test_pending(self):
        job = Job(id=123, job='foo', status='PENDING')
        result = create_job_api_response(job)
        self.assertTrue(result['id'] == 123)
        self.assertTrue(result['job'] == 'foo')
        self.assertTrue(result['status'] == 'PENDING')
    
    def test_fail(self):
        job = Job(id=123, job='foo', status='FAIL', error={ 'message': 'failed' })
        result = create_job_api_response(job)
        self.assertTrue(result['id'] == 123)
        self.assertTrue(result['job'] == 'foo')
        self.assertTrue(result['status'] == 'FAIL')
        self.assertTrue(result['error']['message'] == 'failed')
    
    def test_success(self):
        job = Job(id=123, job='foo', status='SUCCESS', result= {'name' : 'job'})
        result = create_job_api_response(job)
        self.assertTrue(result['id'] == 123)
        self.assertTrue(result['job'] == 'foo')
        self.assertTrue(result['status'] == 'SUCCESS')
        self.assertTrue(result['result']['name'] == 'job')

if __name__ == '__main__':
    unittest.main()