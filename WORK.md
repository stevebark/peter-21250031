## API

API has 2 endpoints.

**POST /**  *Create Job*

**GET /{id}** *Get Job by id*


## Results writer
Created a seperate writer process to listen to the results queue and save them to the db.

## DB Schema

Simple singe job table

```sql
create table jobs
(
	id int not null
		primary key,
	job varchar(200) not null,
	status varchar(200) not null,
	error json null,
	result json null
);

```

## DB migrations
Used FlyWay DB to manage the migrations, if the app was shipped as a docker image then the migration image could be run before the app shipped.


## Sequence Diagram

![sequence](./sequence.png)


## Problems with the code

API both sends writes to the DB and sends to the jobs queue, this wouldn't be acceptable for production because creates a distributed transaction problem, happy to go over alternatives in the review.

Writer and Api have duplicate packages for db and sqs, would have liked to have a single shared package but for the sake of time I left it as is.

Logging is to console, just for demo pusposes

